import { useEffect, useState } from 'react';

function ShoesList() {
  const [shoes, setShoes] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8080/api/shoes/');

    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes)
    }
  }

  useEffect(()=>{
    getData()
  }, [])
  
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Manufacturer</th>
          <th>Color</th>
          <th>Bin</th>
        </tr>
      </thead>
      <tbody>
        {shoes.map(shoe => {
          return (
            <tr key={shoe.href}>
              <td>{ shoe.model_name }</td>
              <td>{ shoe.manufacturer }</td>
              <td>{ shoe.color }</td>
              <td>{ shoe.bin_number }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ShoesList;