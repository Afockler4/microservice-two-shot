import React from 'react'
import { useEffect, useState } from 'react';

function HatsList() {
  const [hats, setHats] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/hats/');

    if (response.ok) {
      const data = await response.json();
      setHats(data.hats)
    }
  }

  useEffect(()=>{
    getData()
  }, [])

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Hat Type</th>
          <th>Location</th>
        </tr>
      </thead>
      <tbody>
        {hats.map(hat => {
          return (
            <tr key={hat.href}>
              <td>{ hat.style_name }</td>
              <td>{ hat.location }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default HatsList;


// function HatList(props) {
//   return (
//     <ul>
//     {props.list.map((item, i) => {
//       return (
//       <li key={i}>{item}</li>
//       );
//     })}
//     </ul>
//   );
// }
