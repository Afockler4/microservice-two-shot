from django.db import models

# Create your models here.
class BinVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()

    def __str__(self):
        return str(self.bin_number)


 


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=150)
    model_name = models.CharField(max_length=150)
    color = models.CharField(max_length=150)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name='shoes',
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.model_name
